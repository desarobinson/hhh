<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Liguidacion</title>
    </head>
    <body>
        <div>
            <h3 align="center" class="title"><strong>Reporte Liquidacion</strong></h3>
        </div>
        <br>
        <div style="margin-top:20px; margin-bottom:15px;">
            <table>
                <tr>
                    <td>
                        <p><b>Empresa: </b></p>
                    </td>
                    <td align="center">
                        <p><strong>{{$company->name}}</strong></p>
                    </td>
                    <td>
                        <p><strong>Fecha: </strong></p>
                    </td>
                    <td align="center">
                        <p><strong>{{date('Y-m-d')}}</strong></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p><strong>Ruc: </strong></p>
                    </td>
                    <td align="center">{{$company->number}}</td>
                    <td>
                        <p><strong>Establecimiento: </strong></p>
                    </td>
                    <td align="center">{{$establishment->address}} - {{$establishment->department->description}} - {{$establishment->district->description}}</td>
                </tr>
            </table>
        </div>
        <br>
        @if(!empty($records))
            <div class="">
                <div class=" ">
                    <table class="">
                        <thead>
                            <tr>
                            <th>#</th>
                                        <th>N°</th>
                                        <th>GRT</th>
                                        <th>Placa</th>
                                        <th>Fecha</th>
                                        <th>Cliente</th>
                                        <th>Servicio/Carga</th>
                                        <th>Comprobante</th>
                                        <th>Monto Flete </th>
                                        <th>Nota de venta </th>
                                        <th>Gastos de Viaje</th>
                                        <th>otros Gastos</th>
                                        <th>Total</th>
                                        <th>Combustible otros</th>
                                        <th>Galones</th>
                                        
                                        <th>Total combus.</th>
                                        
                                     
                                        <th>Destraccion </th>
                                        <th>Estibaje </th>
                                        <th>Total</th>
                                       
                                        <th>Dias.Venc</th>
                                        <th>Pendiente F</th>
                                        <th>Pendiente B</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $total = 0;
                                $repuestostotal = 0;
                                $admin = 0;
                                $sueldo = 0;
                            @endphp
                            @foreach($records as $key => $value)
                                @php
                                    $total_line = $value->total - $value->estibaje - $value->go-$value->gv-$value->lima - $value->hyo - ($value->total *0.04);
                                    $total = $total + $total_line;
                                    $repuestostotal=$value->repuesto;
                                    $admin=$value->admin;
                                    $sueldo=$value->sueldo;
                                @endphp
                                <tr>
                                <td class="celda">{{$loop->iteration}}</td>
                                        <td class="celda">{{$value->id}}</td> 
                                        <td class="celda">{{$value->guiado}}</td>
                                        <td class="celda">{{$value->placa}}</td> 
                                        <td class="celda">{{$value->date_of_issue}}</td> 
                                        
                                        <td class="celda">{{$value->cliente}}</td> 
                                        <td class="celda">{{$value->servicio}}</td>  
                                        <td class="celda">{{$value->serie}} - {{$value->num}}</td> 
                                        <td class="celda">{{$value->total}}</td>  
                                        <td class="celda">{{$value->nota}}</td>
                                        <td class="celda">{{$value->gv}}</td>  
                                        <td class="celda">{{$value->go}}</td>  
                                          
                                        <td class="celda">{{$value->go+$value->gv}}</td>  
                                        <td class="celda">{{$value->hyo}}</td>
                                        <td class="celda">{{$value->galon}}</td>                                       
                                        
                                        <td class="celda">{{$value->lima+$value->hyo}}</td>
                                        
                                        
                                        <td class="celda">{{$value->total * 0.04}}</td>                                       
                                         <td class="celda">{{$value->estibaje}}</td>
                                         <td class="celda">{{$value->total - $value->estibaje - $value->go-$value->gv-$value->lima - $value->hyo - ($value->total *0.04)}}</td>
                                        
                                         <td class="celda">{{$value->vencimiento}}</td>
                                         <td class="celda">{{$value->total-$value->cobrado}}</td>
                                         <td class="celda">{{$value->total-$value->pennota}}</td>
                                         
                                        </tr>
                            @endforeach
                            <tr>
                                <td colspan="8" style="text-align: right;">Total de Fletes: </td>
                                <td>{{number_format($total, 6)}}</td>
                                <td></td>
                                <td></td>
                            </tr>

                            <tr>
                                <td colspan="8" style="text-align: right;">Repuestos:</td>
                                <td>{{number_format($repuestostotal, 6)}}</td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="8" style="text-align: right;">Gastos Administrativos:</td>
                                <td>{{number_format($admin, 6)}}</td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="8" style="text-align: right;">Sueldo:</td>
                                <td>{{number_format($sueldo, 6)}}</td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="8" style="text-align: right;">Total:</td>
                                <td>{{number_format($total-$repuestostotal-$admin-$sueldo, 6)}}</td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        @else
            <div>
                <p>No se encontraron registros.</p>
            </div>
        @endif
    </body>
</html>
