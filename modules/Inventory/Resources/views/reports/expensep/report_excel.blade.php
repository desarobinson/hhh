<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Gastos</title>
    </head>
    <body>
        <div>
            <h3 align="center" class="title"><strong>Reporte Gastos</strong></h3>
        </div>
        <br>
        <div style="margin-top:20px; margin-bottom:15px;">
            <table>
                <tr>
                    <td>
                        <p><b>Empresa: </b></p>
                    </td>
                    <td align="center">
                        <p><strong>{{$company->name}}</strong></p>
                    </td>
                    <td>
                        <p><strong>Fecha: </strong></p>
                    </td>
                    <td align="center">
                        <p><strong>{{date('Y-m-d')}}</strong></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p><strong>Ruc: </strong></p>
                    </td>
                    <td align="center">{{$company->number}}</td>
                    <td>
                        <p><strong>Establecimiento: </strong></p>
                    </td>
                    <td align="center">{{$establishment->address}} - {{$establishment->department->description}} - {{$establishment->district->description}}</td>
                </tr>
            </table>
        </div>
        <br>
        @if(!empty($records))
            <div class="">
                <div class=" ">
                    <table class="">
                        <thead>
                            <tr>
                            <th>#</th>
                                        <th>Numero</th>
                                        <th>Referencia</th>
                                        <th>Fecha</th>
                                        <th>Proveedores</th>
                                        <th>Placa</th>
                                        <th>Descripcion Gasto</th>
                                        <th>Galones</th>
                                        <th>Precio</th>
                                        <th>Motivo</th>                                                                        
                                        <th>TOTAL</th>
                                        <th>Pendiente</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $total = 0;
                                $totalpen = 0;
                            @endphp
                            @foreach($records as $key => $value)
                                @php
                                    $total_line = $value->total;
                                    $total = $total + $total_line;
                                    $totalpen =  $totalpen +($value->total-$value->pendiente);
                                @endphp
                                <tr>
                                <td >{{$loop->iteration}}</td>   
                                        <td >{{$value->id}}</td>  
                                        <td >{{$value->referencia}}</td>  
                                        <td >{{$value->date_of_issue}}</td>                                       
                                        <td >{{$value->proveedor}}</td>                                       
                                        <td >{{$value->placa}}</td>
                                        <td >{{$value->descripcionitem}}</td>
                                        <td >{{$value->galones}}</td>
                                        <td >{{$value->precio}}</td>
                                        <td >{{$value->motivo}}</td>                       
                                       
                                        <td >{{$value->total}}</td>                                   
                                        <td >{{$value->total-$value->pendiente}}</td>      

                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="10" style="text-align: right;">Total</td>
                                <td>{{number_format($total, 6)}}</td>
                                <td> {{number_format($totalpen, 6)}}</td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        @else
            <div>
                <p>No se encontraron registros.</p>
            </div>
        @endif
    </body>
</html>
