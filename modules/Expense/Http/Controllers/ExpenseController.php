<?php

namespace Modules\Expense\Http\Controllers;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Expense\Models\Expense;
use Modules\Expense\Models\ExpenseReason;
use Modules\Expense\Models\ExpensePayment;
use Modules\Expense\Models\ExpenseType;
use Modules\Expense\Models\ExpenseMethodType;
use App\CoreFacturalo\Requests\Inputs\Common\LegendInput;
use Modules\Expense\Models\ExpenseItem;
use Modules\Expense\Http\Resources\ExpenseCollection;
use Modules\Expense\Http\Resources\ExpenseResource;
use Modules\Expense\Http\Requests\ExpenseRequest;
use Illuminate\Support\Str;
use App\Models\Tenant\Person;
use App\Models\Tenant\Catalogs\CurrencyType;
use App\CoreFacturalo\Requests\Inputs\Common\PersonInput;
use App\Models\Tenant\Establishment;
use Illuminate\Support\Facades\DB;
use App\Models\Tenant\Company;
use Modules\Finance\Traits\FinanceTrait; 
use Modules\Order\Models\Vehiculo;
use App\CoreFacturalo\Template;
use Mpdf\Mpdf;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use Exception;
use Mpdf\HTMLParserMode;
use App\Models\Tenant\Configuration;
use App\CoreFacturalo\Helpers\Storage\StorageDocument;
class ExpenseController extends Controller
{

    use FinanceTrait,StorageDocument;

    public function index()
    {
        return view('expense::expenses.index');
    }


    public function create($id = null)
    {
        return view('expense::expenses.form', compact('id'));
    }

    public function columns()
    {
        return [
            'date_of_issue' => 'Fecha de emisión',
            'number' => 'Número',
            'placa' => 'Vehiculo',
            'id' => 'Comprobante',
            'supplier_name' => 'Proveedor',
        ];
    }

/* 
    public function recordsorden($id)
    {
        $recordsorden = Expense::where('order_id', '=', "{$id}")
                            
                            ->latest();

        return $recordsorden;
    } */
    public function recordsorden($id)
    {
        $record = Expense::where('order_id','=',$id)->first();

        return $record;
    }
    public function records(Request $request)
    {
        $records = Expense::where($request->column, 'like', "%{$request->value}%")
                            ->whereTypeUser()
                            ->latest();

        return new ExpenseCollection($records->paginate(config('tenant.items_per_page')));
    }

    public function tables($id)
    {
        $suppliers = $this->table('suppliers');
        $establishment = Establishment::where('id', auth()->user()->establishment_id)->first();
        $lista = Expense::where('order_id','=',$id)
        ->where('state_type_id', '!=','11')
        ->get();
        $currency_types = CurrencyType::whereActive()->get();
        $expense_types = ExpenseType::get();
        $expense_method_types = ExpenseMethodType::all();
        $configuration = Configuration::first();
        $company = Company::active();
        $expense_reasons = ExpenseReason::all();
        $payment_destinations = $this->getBankAccounts();
        $vehiculos = Vehiculo::query()->orderBy('placa')->get()
        ->transform(function($row) { 
            return [
                'id' => $row->id,
                'description' => $row->placa.' - '.$row->marca,
            ];
        });
        return compact('suppliers', 'establishment','currency_types', 'expense_types', 'expense_method_types', 'expense_reasons', 'payment_destinations','vehiculos','company','configuration','lista');
    }

    public function Expense($expense_id)
    {
        $expense = Expense::find($expense_id);

        $total_paid = collect($expense->payments)->sum('payment');
        $total = $expense->total;
        $total_difference = round($total - $total_paid, 2);

        return [
            'number_full' => $expense->number_full,
            'total_paid' => $total_paid,
            'total' => $total,
            'total_difference' => $total_difference
        ];

    }

    public function record($id)
    {
        $record = new ExpenseResource(Expense::findOrFail($id));

        return $record;
    }

    public function store(ExpenseRequest $request)
    {

        $data = self::merge_inputs($request);
        // dd($data);

        $expense = DB::connection('tenant')->transaction(function () use ($data) {

            
            $doc = Expense::updateOrCreate(['id' => $data['id']], $data);

            $doc->items()->delete();

            foreach ($data['items'] as $row)
            {
                $doc->items()->create($row);
            }

            $this->deleteAllPayments($doc->payments);

            foreach ($data['payments'] as $row)
            {
                $record_payment = $doc->payments()->create($row);
                
                if($row['expense_method_type_id'] == 1){
                    $row['payment_destination_id'] = 'cash';
                }

                $this->createGlobalPayment($record_payment, $row);
            }
            $this->setFilename($doc);
            $this->createPdf($doc, "ticket", $doc->filename);   
            return $doc;
        });

        return [
            'success' => true,
            'data' => [
                'id' => $expense->id,
            ],
        ];
    }

    public static function merge_inputs($inputs)
    {

        $company = Company::active();

        $values = [
            'user_id' => auth()->id(),
            'state_type_id' => $inputs['id'] ? $inputs['state_type_id'] : '05',
            'soap_type_id' => $company->soap_type_id,
            'external_id' => $inputs['id'] ? $inputs['external_id'] : Str::uuid()->toString(),
            'supplier' => PersonInput::set($inputs['supplier_id']),
        ];

        $inputs->merge($values);

        return $inputs->all();
    }

    public function table($table)
    {
        switch ($table) {
            case 'suppliers':

                $suppliers = Person::whereType('suppliers')->orderBy('name')->get()->transform(function($row) {
                    return [
                        'id' => $row->id,
                        'description' => $row->number.' - '.$row->name,
                        'name' => $row->name,
                        'number' => $row->number,
                        'identity_document_type_id' => $row->identity_document_type_id,
                        'identity_document_type_code' => $row->identity_document_type->code
                    ];
                });
                return $suppliers;

                break;
            default:

                return [];

                break;
        }
    }

    public function voided ($record)
    {
        try {
            $expense = Expense::findOrFail($record);
            $expense->state_type_id = 11;
            $expense->save();
            return [
                'success' => true,
                'data' => [
                    'id' => $expense->id,
                ],
                'message' => 'Gasto anulado exitosamente',
            ];
        } catch (Exception $e) {
            return [
                'success' => false,
                'data' => [
                    'id' => $record,
                ],
                'message' => 'Falló al anular',
            ];
        }
    }
  

    public function createPdf($expense = null, $format_pdf = null, $filename = null) {

        ini_set("pcre.backtrack_limit", "5000000");
        $template = new Template();
        $pdf = new Mpdf();
        $format_pdf="ticket";
        $document = ($expense != null) ? $expense : $this->expense;
        $company = Company::active();
        $filename = ($filename != null) ? $filename : $this->expense->filename;

        $base_template = Configuration::first()->formats;

        $html = $template->pdf($base_template, "expense", $company, $document, $format_pdf);
        Log::debug($html);

        $pdf_font_regular = config('tenant.pdf_name_regular');
        $pdf_font_bold = config('tenant.pdf_name_bold');

        if ($pdf_font_regular != false) {
            $defaultConfig = (new ConfigVariables())->getDefaults();
            $fontDirs = $defaultConfig['fontDir'];

            $defaultFontConfig = (new FontVariables())->getDefaults();
            $fontData = $defaultFontConfig['fontdata'];

            // $pdf = new Mpdf([
            //     'fontDir' => array_merge($fontDirs, [
            //         app_path('CoreFacturalo'.DIRECTORY_SEPARATOR.'Templates'.
            //                                     DIRECTORY_SEPARATOR.'pdf'.
            //                                     DIRECTORY_SEPARATOR.$base_template.
            //                                     DIRECTORY_SEPARATOR.'font')
            //     ]),
            //     'fontdata' => $fontData + [
            //         'custom_bold' => [
            //             'R' => $pdf_font_bold.'.ttf',
            //         ],
            //         'custom_regular' => [
            //             'R' => $pdf_font_regular.'.ttf',
            //         ],
            //     ]
            // ]);

           
            $pdf = new Mpdf([
                'mode' => 'utf-8',
                'format' => [
                    76,
                    100],
                'margin_top' => 0,
                'margin_right' => 2,
                'margin_bottom' => 0,
                'margin_left' => 2
            ]);

            
        }
        Log::debug("Aquiiiii");
        $path_css = app_path('CoreFacturalo'.DIRECTORY_SEPARATOR.'Templates'.
                                             DIRECTORY_SEPARATOR.'pdf'.
                                             DIRECTORY_SEPARATOR.$base_template.
                                             DIRECTORY_SEPARATOR.'style.css');

        $stylesheet = file_get_contents($path_css);

        $pdf->WriteHTML($stylesheet, HTMLParserMode::HEADER_CSS);
        $pdf->WriteHTML($html, HTMLParserMode::HTML_BODY);

        if ($format_pdf != 'ticket') {
            if(config('tenant.pdf_template_footer')) {
                $html_footer = $template->pdfFooter($base_template,$document);
                $pdf->SetHTMLFooter($html_footer);
            }
        }

        $this->uploadFile($filename, $pdf->output('', 'S'), 'purchase');
    }

    public function uploadFile($filename, $file_content, $file_type) {
        $this->uploadStorage($filename, $file_content, $file_type);
        Log::debug($filename);
        Log::debug($file_content);
        Log::debug($file_type);
    }
    private function setFilename($expense){

        $name = [$expense->series,$expense->number,$expense->id,date('Ymd')];
        $expense->filename = join('-', $name);
        $expense->save();
        Log::debug($expense);
        Log::debug( $expense->filename);
       
    }

    public function toPrint($external_id, $format) {
        $expense = Expense::where('external_id', $external_id)->first();

        if (!$expense) throw new Exception("El código {$external_id} es inválido, no se encontro el pedido relacionado");

        $this->reloadPDF($expense, $format, $expense->filename);
        $temp = tempnam(sys_get_temp_dir(), 'expense');

        file_put_contents($temp, $this->getStorage($expense->filename, 'expense'));

        return response()->file($temp);
    }

    private function reloadPDF($expense, $format, $filename) {
        $this->createPdf($expense, $format, $filename);
    }
    public function xml2array ( $xmlObject, $out = array () )
    {
        foreach ((array) $xmlObject as $index => $node) {
            $out[$index] = ( is_object ( $node ) ) ?  $this->xml2array($node) : $node;
        }
        return $out;
    }

    function XMLtoArray($xml) {
        $previous_value = libxml_use_internal_errors(true);
        $dom = new \DOMDocument('1.0', 'UTF-8');
        $dom->preserveWhiteSpace = false;
        $dom->loadXml($xml);
        libxml_use_internal_errors($previous_value);
        if (libxml_get_errors()) {
            return [];
        }
        return $this->DOMtoArray($dom);
    }
    
    public function DOMtoArray($root) {
        $result = array();

        if ($root->hasAttributes()) {
            $attrs = $root->attributes;
            foreach ($attrs as $attr) {
                $result['@attributes'][$attr->name] = $attr->value;
            }
        }

        if ($root->hasChildNodes()) {
            $children = $root->childNodes;
            if ($children->length == 1) {
                $child = $children->item(0);
                if (in_array($child->nodeType,[XML_TEXT_NODE,XML_CDATA_SECTION_NODE])) {
                    $result['_value'] = $child->nodeValue;
                    return count($result) == 1
                        ? $result['_value']
                        : $result;
                }

            }
            $groups = array();
            foreach ($children as $child) {
                if (!isset($result[$child->nodeName])) {
                    $result[$child->nodeName] = $this->DOMtoArray($child);
                } else {
                    if (!isset($groups[$child->nodeName])) {
                        $result[$child->nodeName] = array($result[$child->nodeName]);
                        $groups[$child->nodeName] = 1;
                    }
                    $result[$child->nodeName][] = $this->DOMtoArray($child);
                }
            }
        }
        return $result;
    }
}
