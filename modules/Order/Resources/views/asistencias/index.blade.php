@extends('tenant.layouts.app')

@section('content')

    <tenant-asistencias-index :type-user="{{json_encode(Auth::user()->type)}}"></tenant-asistencias-index>

@endsection
